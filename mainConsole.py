import sqlite3, sys

running = True

def addIngredient():
    #Get info for ingredient
    name = input('Ingredient name(t):')
    image = input('Image name(t/f):')
    description = input('description(t/f):')
    type = input('Type(t):')
    allergiesList = input('Allergies(t/f):')
    id = input('ID(t):')

    #Format info
    if(image[-4:-1] == '.txt'):
        name = name[0:-5]
    if(description[-4:1] == '.txt'):
        description = description[0:-5]
    if(allergiesList[-4:1] == '.text'):
        allergiesList = allergiesList[0:-5]
    
    #Save ingredient
    conn = sqlite3.connect('data.db')
    c = conn.cursor()

    c.execute(f"INSERT INTO ingredients VALUES ('{name}','{image}','{description}','{type}','{allergiesList}','{id}')")
    conn.commit()
    conn.close()


#def addRecipe():


def showAllIngredients():
    conn = sqlite3.connect('data.db')
    c = conn.cursor()

    c.execute("SELECT * FROM ingredients")
    print(c.fetchall())

    conn.commit()
    conn.close()


def showAllRecipies():
    conn = sqlite3.connect('data.db')
    c = conn.cursor()

    c.execute("SELECT * FROM recipies")
    print(c.fetchall())

    conn.commit()
    conn.close()

#def showIngredient():

#def showRecipie():

#def editIngredient():

#def editRecipe():

#def createDb():
    #ingredients
        #name
        #image
        #description
        #type
        #allergies
        #id   

    #recipies
        #name
        #IngredientIDs
        #instructions
        #ingredientamounts
        #image

#def deleteDb():
    #conn = sqlite3.connect('data.db')
    #c = conn.cursor()

    #c.execute("DELETE FROM <table>")

    #conn.commit()
    #conn.close()

def exit():
    sys.exit()


def main():
    choice = 0
    while(running):
        print('-----Kokbok Console Application-----\n\n')
        print('1.Add ingredient')
        print('2.Add recipe')
        print('3.Show all ingredients')
        print('4.Show all recipies')
        print('5.Show ingredient')
        print('6.Show recipe')
        print('7.Edit ingredient')
        print('8.Edit recipe')
        print('9.Create database')
        print('10.Delete database')
        print('11.Exit\n')
        choice = input(':')

        if(choice == '1'):
            addIngredient()
        elif(choice == '2'):
            addRecipe()
        elif(choice == '3'):
            showAllIngredients()
        elif(choice == '4'):
            showAllRecipies()
        elif(choice == '5'):
            showIngredient()
        elif(choice == '6'):
            showRecipie()
        elif(choice == '7'):
            editIngredient()
        elif(choice == '8'):
            editRecipe()
        elif(choice == '9'):
            createDb()
        elif(choice == '10'):
            deleteDb()
        elif(choice == '11'):
            exit()
        else:
            print('Incorrect input, please try again')


main()